export default {
  head: {
    titleTemplate: 'challenge',
    title: 'challenge',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  css: [
    '~/assets/main.css'
  ],
  components: true,
  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify'
  ],

  vuetify: {
    theme: {
      dark: false,
      themes: {
        light: {
          background: '#e5e5e5',
          primary: '#484848'
        }
      }
    }
  }
}
