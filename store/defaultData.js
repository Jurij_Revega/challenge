export const defaultVacancies = [
  {
    id: 1,
    title: 'Title 1',
    description: 'Description 1',
    dates: [{
      date: '2020-11-10',
      startTime: '12:00',
      endTime: '13:00',
      price: 100,
      type: 'Consultation'
    }]
  },
  {
    id: 2,
    title: 'Title 2',
    description: 'Description 2',
    dates: [{
      date: '2020-11-12',
      startTime: '10:00',
      endTime: '12:00',
      price: 75,
      type: 'Consultation'
    },
    {
      date: '2020-11-14',
      startTime: '10:00',
      endTime: '12:00',
      price: 200,
      type: 'Consultation'
    }]
  }
]

export const defaultVacancy = {
  id: '',
  title: '',
  description: '',
  dates: []
}

export const defaultShift = {
  date: '',
  startTime: '',
  endTime: '',
  price: '',
  type: ''
}
