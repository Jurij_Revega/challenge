import { defaultVacancies, defaultVacancy, defaultShift } from '~/store/defaultData'

const clone = function (obj) {
  return JSON.parse(JSON.stringify(obj))
}

export const state = () => ({
  defaultVacancies,
  defaultVacancy,
  defaultShift,

  vacancies: [],
  selectedVacancy: {}
})

export const mutations = {
  SET_VACANCIES (state, data) {
    state.vacancies = data
  },
  SET_SELECTED_VACANCY (state, data) {
    state.selectedVacancy = data
  },
  CLEAR_SELECTED_VACANCY (state) {
    state.selectedVacancy = defaultVacancy
  },
  ADD_NEW_VACANCY (state, data) {
    state.vacancies.unshift(data)
  },
  EDIT_VACANCY (state, data) {
    state.vacancies = state.vacancies.map((el) => {
      if (el.id === data.id) {
        return data
      }
      return el
    })
  },
  DELETE_VACANCY (state, data) {
    state.vacancies = state.vacancies.filter((el) => {
      if (el.id === data.id) {
        return
      }
      return true
    })
  }
}

export const actions = {
  setExampleData ({ commit, state }) {
    const defaultVacanciesClone = clone(state.defaultVacancies)
    commit('SET_VACANCIES', defaultVacanciesClone)
  }
}

export const getters = {
  getPrices (state) {
    let prices = []
    const pricesDeep = state.vacancies.map((el) => { return el.dates.map(item => item.price) })
    pricesDeep.forEach((el) => { prices = prices.concat(el) })
    return prices
  },
  getMinPrice (state, getters) {
    return Math.min(...getters.getPrices)
  },
  getMaxPrice (state, getters) {
    return Math.max(...getters.getPrices)
  },
  getVacanciesAllDates (state) {
    const clone = JSON.parse(JSON.stringify(state.vacancies))
    const datesDeep = clone.map((el) => {
      el.dates.forEach((date) => { date.id = el.id })
      return el.dates
    })
    let dates = []
    datesDeep.forEach((el) => { dates = dates.concat(el) })
    return dates
  }
}
